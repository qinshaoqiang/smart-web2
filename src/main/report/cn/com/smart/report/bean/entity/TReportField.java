package cn.com.smart.report.bean.entity;

import cn.com.smart.bean.BaseBean;
import cn.com.smart.bean.DateBean;
import cn.com.smart.web.bean.LabelValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mixsmart.utils.CollectionUtils;
import com.mixsmart.utils.StringUtils;

import javax.persistence.*;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * 报表字段实体类
 * @author lmq  2017年9月10日
 * @version 1.0
 * @since 1.0
 */
@Entity
@Table(name="t_report_field")
public class TReportField implements BaseBean, DateBean {
    
    /**
     * 
     */
    private static final long serialVersionUID = 4789667416867643754L;

    private String id;
    
    private String reportId;
    
    private String title;
    
    private String width;
    
    private String url;
    
    private String openUrlType;
    
    private String paramName;
    
    private String paramValue;
    
    private String searchName;

    /**
     * 搜索插件类型；如：text，select， checkbox等
     */
    private String searchPluginType;

    private String searchPluginUrl;

    /**
     * 数据格式
     * searchPluginType 字段值为日期控件，则该字段为日期格式；其他插件该字段为空
     */
    private String searchDataFormat;

    //排序字段名称
    private String sortFieldName;
    
    private String customClass;
    
    private Integer sortOrder;
    
    private Date createTime;

    private String searchCustomOptions;

    //非持久化对象
    private List<LabelValue> labelValues;
    //非持久化属性
    private String searchValue;

    @Id
    @Column(name="id", length=50)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name="report_id", length=50, nullable=false)
    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    @Column(name="title", length=127, nullable=false)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name="width", length = 50)
    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    @Column(name="url", length = 255)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name="open_url_type", length = 127)
    public String getOpenUrlType() {
        return openUrlType;
    }

    public void setOpenUrlType(String openUrlType) {
        this.openUrlType = openUrlType;
    }

    @Column(name="param_name", length=255)
    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    @Column(name="param_value", length = 127)
    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    @Column(name="search_name", length=127)
    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    @Column(name = "search_plugin_type", length = 50)
    public String getSearchPluginType() {
        return searchPluginType;
    }

    public void setSearchPluginType(String searchPluginType) {
        this.searchPluginType = searchPluginType;
    }

    @Column(name = "search_plugin_url", length = 255)
    public String getSearchPluginUrl() {
        return searchPluginUrl;
    }

    public void setSearchPluginUrl(String searchPluginUrl) {
        this.searchPluginUrl = searchPluginUrl;
    }

    @Column(name = "search_data_format", length = 127)
    public String getSearchDataFormat() {
        return searchDataFormat;
    }

    public void setSearchDataFormat(String searchDataFormat) {
        this.searchDataFormat = searchDataFormat;
    }

    @Column(name = "sort_field_name", length = 50)
    public String getSortFieldName() {
        return sortFieldName;
    }

    public void setSortFieldName(String sortFieldName) {
        this.sortFieldName = sortFieldName;
    }

    @Column(name="sort_order")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
    
    @Column(name="custom_class", length=255)
    public String getCustomClass() {
        return customClass;
    }

    public void setCustomClass(String customClass) {
        this.customClass = customClass;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_time", updatable=false)
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Column(name = "search_custom_options", length = 500)
    public String getSearchCustomOptions() {
        if(StringUtils.isEmpty(searchCustomOptions) && CollectionUtils.isNotEmpty(labelValues)) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                searchCustomOptions = mapper.writeValueAsString(labelValues);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return searchCustomOptions;
    }

    public void setSearchCustomOptions(String searchCustomOptions) {
        this.searchCustomOptions = searchCustomOptions;
    }



    @Override
    @Transient
    public String getPrefix() {
        return "RF";
    }

    @Transient
    public List<LabelValue> getLabelValues() {
        if(CollectionUtils.isEmpty(labelValues) && StringUtils.isNotEmpty(searchCustomOptions)) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                labelValues = mapper.readValue(searchCustomOptions, new TypeReference<List<LabelValue>>(){});
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return labelValues;
    }

    public void setLabelValues(List<LabelValue> labelValues) {
        this.labelValues = labelValues;
    }

    @Transient
    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }
}
