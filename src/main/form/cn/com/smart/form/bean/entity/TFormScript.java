package cn.com.smart.form.bean.entity;

import cn.com.smart.bean.DateBean;
import cn.com.smart.bean.LogicalDeleteSupport;

import javax.persistence.*;
import java.util.Date;

/**
 * @author 乌草坡 2019-09-10
 * @since 1.0
 */
@Entity
@Table(name = "t_form_script")
public class TFormScript extends LogicalDeleteSupport implements DateBean {

    private String id;

    private String formId;

    private String initFun;

    private String submitBeforeFun;

    private String submitAfterFun;

    private String userId;

    private Date createTime;

    @Id
    @Column(name = "id", length = 50)
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "form_id", length = 50, nullable = false)
    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    @Lob
    @Column(name="init_fun",columnDefinition="MEDIUMTEXT")
    public String getInitFun() {
        return initFun;
    }

    public void setInitFun(String initFun) {
        this.initFun = initFun;
    }

    @Lob
    @Column(name="submit_before_fun",columnDefinition="MEDIUMTEXT")
    public String getSubmitBeforeFun() {
        return submitBeforeFun;
    }

    public void setSubmitBeforeFun(String submitBeforeFun) {
        this.submitBeforeFun = submitBeforeFun;
    }

    @Lob
    @Column(name="submit_after_fun",columnDefinition="MEDIUMTEXT")
    public String getSubmitAfterFun() {
        return submitAfterFun;
    }

    public void setSubmitAfterFun(String submitAfterFun) {
        this.submitAfterFun = submitAfterFun;
    }

    @Column(name = "user_id", length = 50, nullable = false, updatable = false)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_time", updatable = false)
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
